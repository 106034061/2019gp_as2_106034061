﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomContorller : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject BoomEffect;    

    void Start()
    {
        Destroy(gameObject, 5);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(GameObject other)
    {
        if(other.gameObject.tag != "Player"){
            GameObject exp = GameObject.Instantiate(BoomEffect, Vector3.zero, Quaternion.identity) as GameObject;
            exp.transform.position = this.gameObject.transform.position;
            GameObject.Destroy(exp, 3);
            Destroy(gameObject);
        }        

    }
}
