﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour
{
    public AudioClip Music;
    public AudioSource audioSource;
    // void Start () {
    //     audioSource.PlayOneShot(Music);
    //     GameObject.DontDestroyOnLoad(audioSource);
    //     GameObject.DontDestroyOnLoad(Music);

    // }
    public void OnButtonClick(){
        Debug.Log("Start");
        SceneManager.LoadScene("Game_1", LoadSceneMode.Single);
    }
    public void QuitClick(){
        Debug.Log("Quit");
        SceneManager.LoadScene("End", LoadSceneMode.Single);
    }
    public void ReplayClick(){
        Debug.Log("Replay");
        SceneManager.LoadScene("Start", LoadSceneMode.Single);
    }
}
